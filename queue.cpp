#include "queue.h"
#include <iostream>
/*
this function creates the queue
input: pointer to the queue struct and the size of the queue
output: none
*/
void initQueue(queue* q, unsigned int size)
{
	q->qArr = new unsigned int[size];
	q->size = size;
	q->end = 0;
	q->begining = 0;
	q->qArr[q->end] = END;
}
/*
this function cleans the queue
input: pointer to the queue struct
output: none
*/
void cleanQueue(queue* q)
{
	int i = 0;
	for (i = 0; i < q->end; i++)
	{
		dequeue(q);
	}
	delete[] q->qArr;
}
/*
this function adds a new number to the queue
input: pointer to the queue struct and the new number we want to add
output: none
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (q->end == q->size - 1)
	{
		std::cout << "Queue full";
	}
	else if (q->qArr[q->begining] == END)
	{
		q->qArr[q->begining] = newValue;
		q->end++;
		q->qArr[q->end] = END;
	}
	else
	{
		q->qArr[q->end] = newValue;
		q->end++;
		q->qArr[q->end] = END;
	}
}
/*
this function removes the first number from the queue and returns it
input: pointer to the queue struct
output: the top number that was in the queue
*/
int dequeue(queue* q)
{
	int returner = 0;
	int i = 0;
	if (q->begining == q->end) 
	{
		std::cout << "Queue empty";
		returner = END;
	}
	else 
	{
		returner = q->qArr[q->begining];
		for (i = 0; i < q->end - 1; i++)
		{
			q->qArr[i] = q->qArr[i + 1];
		}
		q->end--;
	}
	return returner;
}
