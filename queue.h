#ifndef QUEUE_H

#define QUEUE_H
#define END -1

/* a queue contains positive integer values. */
typedef struct queue
{
	int size;
	int begining;
	int end;
	unsigned int* qArr;
} queue;

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);
void print(queue* q);
void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty

#endif /* QUEUE_H */